# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 14:39:04 2023

@author: Saurabh Shelar
"""

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

def send_email(to, cc, frm, subject, body, attachments, signature):
    # Create a multipart message container
    msg = MIMEMultipart()
    msg['To'] = ', '.join(to)
    msg['Cc'] = ', '.join(cc)
    msg['From'] = frm
    msg['Subject'] = subject

    # Add the message body as a MIME text part
    body_part = MIMEText(body, 'plain')
    msg.attach(body_part)

    # Add the attachments to the message
    for attachment in attachments:
        with open(attachment, 'rb') as f:
            file_data = f.read()
        file_part = MIMEApplication(file_data)
        file_part.add_header('Content-Disposition', 'attachment', filename=attachment)
        msg.attach(file_part)

    # Add the signature to the message
    signature_part = MIMEText(signature, 'plain')
    msg.attach(signature_part)

    # Connect to the SMTP server and send the message
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login('personal_mail@gmail.com', 'app_code')
    recipients = to + cc
    server.sendmail(frm, recipients, msg.as_string())
    server.quit()
    
    
to = ['recipient_mail@gmail.com']
cc = ['recepient_mail@gmail.com']
frm = 'personal_mail76@gmail.com'
subject = 'This is test mail'
body = 'This is a test email.'
attachments = [r'file_path']
signature = 'Best regards,\nYour_name'

send_email(to, cc, frm, subject, body, attachments, signature)
